require 'state_machine'
require 'highline/import'

class PopVehicle
  state_machine :initial => :parked do

    event :ignite do
      transition [:parked] => :idling
    end

    event :park do
      transition [:idling] => :parked
    end

    event :gear_up do
      transition [:idling] => :first_gear,
                 [:first_gear] => :second_gear,
                 [:second_gear] => :third_gear,
                 [:third_gear] => :crashed
    end

    event :gear_down do
      transition [:first_gear] => :idling,
                 [:second_gear] => :first_gear,
                 [:third_gear] => :second_gear
    end

    event :crashed do
      transition [:first_gear, :second_gear, :third_gear] => :crashed
    end

    event :repair do
      transition [:crashed] => :parked
    end

  end

end

vehicle = PopVehicle.new

while true do
  puts "car is #{vehicle.state}"
  puts "can do #{vehicle.state_events}"
  input = ask "-> Action ?: "
  vehicle.send(input)
end
