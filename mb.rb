deck = []
descriptions = [
  ["ACCIDENT",3, "ATTAQUE"],
  ["PANNE",3, "ATTAQUE"],
  ["CREVAISON",3, "ATTAQUE"],
  ["LIMIT_VIT",4, "ATTAQUE"],
  ["FEU_ROUGE",5, "ATTAQUE"],
  ["ESSENCE",6, "DEFENSE"],
  ["REPARATION",6, "DEFENSE"],
  ["ROUE_SEC",6, "DEFENSE"],
  ["LIMIT_FIN",6, "DEFENSE"],
  ["FEU_VERT",14, "DEFENSE"],
  ["AS_DU_VOLANT",1, "BOTTE"],
  ["PRIO",1, "BOTTE"],
  ["CITERNE",1, "BOTTE"],
  ["INCREVABLE",1, "BOTTE"],
  ["25KM",10, "DISTANCE"],
  ["50KM",10, "DISTANCE"],
  ["75KM",10, "DISTANCE"],
  ["100KM",12, "DISTANCE"],
  ["200KM",4, "DISTANCE"]
]

descriptions.each do |description|
  for i in 1..description[1] do
    une_carte=Carte.new
    une_carte.libelle=description[0]
    une_carte.type=description[2]
    mon_deck.add_card(une_carte)
  end
end

class Deck
  attr_accessor :cards
  def initialize()
    puts "New deck created !"
    @cards=[]
  end
  def add_card(card)
    @cards << card
  end
  def melange()
    puts "card shuffled ..."
    @cards.shuffle!
  end
  def afficher_jeu()
    for i in 1..(@cards.count) do
      puts @cards[i-1].libelle
    end
  end
  def tirer_carte()
    i=0
    while i<@cards.count && @cards[i].state != "FREE"  do
      i=i+1
    end
    if(i==(@cards.count)) then
      puts "no more card.."
    else
      @cards[i].state="TAKEN"
      puts @cards[i].libelle
    end
  end
end

class Carte
  attr_accessor :libelle, :type, :state
  # state = FREE / TAKEN / PLAYED / DROPPED
  def initialize()
    @state="FREE"
  end
end

class Player
  attr_accessor :name, :hand
  def initialize(*args)
    @hand=[]
    @name= args[0]

    puts "Cards for #{self.name} ="
    
    for i in 1..6 do
      @hand << $mon_deck.tirer_carte
      puts $mon_deck.cards[@hand[i-1]].libelle
    end
  end
end



$mon_deck=Deck.new
mon_deck.melange

playerA = Player.new("Joueur A")
playerB = Player.new("Joueur B")
playerC = Player.new("Joueur C")
playerD = Player.new("Joueur D")
